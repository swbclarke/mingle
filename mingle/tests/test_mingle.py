import os
from unittest import TestCase
import mingle


def get_data_folder():
    return os.path.join(os.path.dirname(__file__), 'res')


class TestMingle(TestCase):
    def setUp(self):
        data_dir = get_data_folder()
        files = ['file1.txt', 'file2.txt']
        self.files = map(lambda x: os.path.join(data_dir, x), (f for f in files))

    def test_generator_ordering(self):
        for index, (line, filename) in enumerate(mingle.mingled(self.files)):
            self.assertIn("Event {}".format(index+1), line)